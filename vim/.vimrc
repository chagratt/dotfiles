" -----------------------------------------------------------------------------
" Vim general settings
" -----------------------------------------------------------------------------
set nocompatible
set wildmenu                    " better command line completion
set wildmode=longest:list,full
set number                      " Show line numbers
set showmatch                   " Show matching bracket/parenthesis/etc
set showcmd                     " Show current command
set encoding=utf-8              " The encoding displayed.
set fileencoding=utf-8          " The encoding written to file.
set autoread                    " Auto reload file after external command
set title                       " Change terminal title
set nowrap                      " Don't wrap long lines
set ttyfast                     " we have a fast terminal
set backspace=indent,eol,start  " Allow backspacing over everything in insert mode
set vb                          " turn on the "visual bell" - which is much quieter than the "audio blink"
set ruler                       " show the cursor position all the time
set showtabline=2               " Always show tabs
set laststatus=2                " make the last line where the status is two lines deep so you can see status always

" Indentation
set autoindent                  " Copy indentation from previous line"
set smartindent                 " Insert new level of indentation
set smarttab                    " Better tabs
set tabstop=4                   " Columns a tab counts for
set softtabstop=4               " Columns a tab inserts in insert mode
set shiftwidth=4                " Columns inserted with the reindent operations
set shiftround                  " Always indent by multiple of shiftwidth
set expandtab                   " Always use spaces instead of tabs

" Search
set hlsearch                    " hilghlight search results. Type :nohl to remove until next search
set incsearch                   " Search as typing"
set ignorecase                  " Search insensitive
set smartcase                   " ... unless search contains uppercase letter

" Scroll
set sidescrolloff=5             " Keep at least 5 lines left/right
set scrolloff=5                 " Keep at least 5 lines above/below
set sidescroll=5

" Invisible characters
set list
set listchars=tab:→\ ,eol:\ ,trail:·,nbsp:¤
set listchars+=precedes:«,extends:»

" Splits
set splitbelow                  " Horizontal split below
set splitright                  " Vertical split right

" Make completion menu behave like an IDE
set completeopt=longest,menuone,preview

syntax on

filetype plugin indent on

" -----------------------------------------------------------------------------
" Custom statusline
" -----------------------------------------------------------------------------
" layout
set statusline=\ %<%F           " file path
set statusline+=\ %m            " modified flag
set statusline+=\ %r            " read only flag
set statusline+=%=              " on the right from now
set statusline+=%{g:gitbranch}  " git branch
set statusline+=\ %y            " file type
set statusline+=\ %{strlen(&fenc)?&fenc:'none'},%{&ff} " encoding, EOL type
set statusline+=\ \ %{strftime(\"%Y-%m-%d\ %H:%M\",getftime(expand(\"%:p\")))} " last write time
set statusline+=\ \ %l/%L:%c\  " line number, column and scrolling percent

" functions
" inspired by :
" https://www.reddit.com/r/vim/comments/pq3xwa/how_to_get_the_git_branch_in_the_statusline/
function StatuslineGitBranch()
    let l:gitrevparse=trim(system("git -C " . expand("%:h") . " branch --show-current 2>/dev/null"))
    if strlen(l:gitrevparse) > 0
        return "(".substitute(l:gitrevparse, '\n', '', 'g').")"
    else
        return ""
    endif
endfunction

augroup GetGitBranch
  autocmd!
  autocmd VimEnter,WinEnter,BufEnter * let g:gitbranch = StatuslineGitBranch()
augroup END

" -----------------------------------------------------------------------------
" Key mappings
" -----------------------------------------------------------------------------

" Smart way to move between windows (<ctrl>j etc.):
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Leader key
let g:mapleader = ","

" buffers : next an previous
nnoremap <Leader>bn :bn<CR>
nnoremap <Leader>bp :bp<CR>
nnoremap <Leader>bd :bdelete<CR>

" F7 turns Fr spelling on
noremap <silent> <F7> <Esc>:silent setlocal spell! spelllang=fr<CR>

" Custom Shortcuts for grammalecte
nnoremap <Leader>g :GrammalecteCheck<CR>
nnoremap <Leader>G :GrammalecteClear<CR>

" Shorcut for :nohl
nnoremap <Leader>h :nohl<CR>

" For notes-taking : overwrite the `gf` keys command and
" open path in new tab and create the file even if it does
" not exists
noremap <Leader>gf :tabnew <cfile><cr>

" Shortcuts for vimdiff
" next diff
nnoremap <Leader>dn ]c
" previous diff
nnoremap <Leader>dp [c
" update diff
nnoremap <Leader>du :diffupdate<CR>

" QuickFix windows :
" Show the quickfix window (22 lines height)
nnoremap <Leader>co :copen 22<CR>
" Show vertical quickfix (90chr wide)
nnoremap <Leader>cO :vertical botright copen 90<CR>
" Hide the quickfix window
nnoremap <Leader>cc :cclose<CR>

" -----------------------------------------------------------------------------
" Plugins config
" -----------------------------------------------------------------------------

let g:delimitMate_expand_cr = 1

" grammar cheking plugin
set spelllang=fr
let g:grammalecte_cli_py='/usr/bin/grammalecte-cli'

" -----------------------------------------------------------------------------
" External tools config
" -----------------------------------------------------------------------------
" search with ripgrep
if executable("rg")
    " linux :
    set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case
    " if windows w/ git-bash (see: https://github.com/BurntSushi/ripgrep/issues/275) :
    "set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case\ --path-separator\ //
    set grepformat=%f:%l:%c:%m
endif
" call ripgrep in a sub-shell and capture the output,
" rather than the original parent-shell call
function! Grep(...)
    return system(join([&grepprg] + [expandcmd(join(a:000, ' '))], ' '))
endfunction
" search and opens quickfix windows
command! -nargs=+ -complete=file_in_path -bar Find cgetexpr Grep(<f-args>) | copen 22

" -----------------------------------------------------------------------------
" Auto commands
" -----------------------------------------------------------------------------

" when in _notes, change working directory
autocmd BufRead,BufNewFile *.md if expand("%:p:h") =~ '/_notes$' | silent! cd %:p:h | endif

function! QuickfixMapping()
  " Go to the previous location and stay in the quickfix window
  nnoremap <buffer> K :cprev<CR>zz<C-w>w
  " Go to the next location and stay in the quickfix window
  nnoremap <buffer> J :cnext<CR>zz<C-w>w
  " Open selected result in a new tab (in fact, first a split, then change it
  " to a new tab)
  nnoremap <buffer> <Enter> <C-W><Enter><C-W>T
endfunction

augroup quickfix_group
    autocmd!
    autocmd filetype qf call QuickfixMapping()
augroup END

" force some filetypes colors
autocmd BufRead,BufNewFile *.md set filetype=markdown
autocmd BufNewFile,BufRead *.patch set filetype=diff
autocmd BufNewFile,BufRead *.diff set filetype=diff

" -----------------------------------------------------------------------------
" Color Scheme
" Copied (and adapted) from xoria256 :
" https://www.vim.org/scripts/script.php?script_id=2140
" -----------------------------------------------------------------------------
set background=dark
highlight clear
syntax reset
set t_Co=256
hi Normal       ctermfg=252 guifg=#d0d0d0 ctermbg=234 guibg=#1c1c1c cterm=none gui=none"}}}
hi Cursor                                 ctermbg=214 guibg=#ffaf00
hi CursorColumn                           ctermbg=238 guibg=#444444
hi CursorLine                             ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi ColorColumn  ctermbg=235 guibg=#2c2d27
hi Error        ctermfg=15  guifg=#ffffff ctermbg=1   guibg=#800000
hi ErrorMsg     ctermfg=15  guifg=#ffffff ctermbg=1   guibg=#800000
hi FoldColumn   ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212
hi Folded       ctermfg=255 guifg=#eeeeee ctermbg=60  guibg=#5f5f87
hi IncSearch    ctermfg=0   guifg=#000000 ctermbg=223 guibg=#ffdfaf cterm=none gui=none
hi LineNr       ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212
hi MatchParen   ctermfg=188 guifg=#dfdfdf ctermbg=68  guibg=#5f87df cterm=bold gui=bold
" TODO
" hi MoreMsg
hi NonText      ctermfg=247 guifg=#9e9e9e ctermbg=233 guibg=#121212 cterm=bold gui=bold
hi Pmenu        ctermfg=0   guifg=#000000 ctermbg=250 guibg=#bcbcbc
hi PmenuSel     ctermfg=255 guifg=#eeeeee ctermbg=243 guibg=#767676
hi PmenuSbar                              ctermbg=252 guibg=#d0d0d0
hi PmenuThumb   ctermfg=243 guifg=#767676
hi Search       ctermfg=0   guifg=#000000 ctermbg=149 guibg=#afdf5f
hi SignColumn   ctermfg=248 guifg=#a8a8a8
hi SpecialKey   ctermfg=77  guifg=#5fdf5f
" hi SpellBad     ctermfg=160 guifg=fg      ctermbg=bg                cterm=underline               guisp=#df0000
hi SpellBad      ctermfg=252                         ctermbg=160
hi SpellCap     ctermfg=189 guifg=#dfdfff ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi SpellRare    ctermfg=168 guifg=#df5f87 ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi SpellLocal   ctermfg=98  guifg=#875fdf ctermbg=bg  guibg=bg      cterm=underline gui=underline
hi StatusLine   ctermfg=15  guifg=#ffffff ctermbg=239 guibg=#4e4e4e cterm=bold gui=bold
hi StatusLineNC ctermfg=249 guifg=#b2b2b2 ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi StatusLineTerm ctermfg=15  guifg=#ffffff ctermbg=239 guibg=#4e4e4e cterm=bold gui=bold
hi StatusLineTermNC ctermfg=249 guifg=#b2b2b2 ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi TabLine  ctermfg=fg  guifg=fg      ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi TabLineSel   ctermfg=15 guifg=#9e9e9e ctermbg=233 guibg=#121212 cterm=bold gui=bold
hi TabLineFill  ctermfg=fg  guifg=fg      ctermbg=237 guibg=#3a3a3a cterm=none gui=none
" FIXME
hi Title        ctermfg=225 guifg=#ffdfff
hi Todo         ctermfg=0   guifg=#000000 ctermbg=184 guibg=#dfdf00
hi Underlined   ctermfg=39  guifg=#00afff                           cterm=underline gui=underline
hi VertSplit    ctermfg=237 guifg=#3a3a3a ctermbg=237 guibg=#3a3a3a cterm=none gui=none
" hi VIsualNOS    ctermfg=24  guifg=#005f87 ctermbg=153 guibg=#afdfff cterm=none gui=none
" hi Visual       ctermfg=24  guifg=#005f87 ctermbg=153 guibg=#afdfff
hi Visual       ctermfg=255 guifg=#eeeeee ctermbg=96  guibg=#875f87
" hi Visual       ctermfg=255 guifg=#eeeeee ctermbg=24  guibg=#005f87
hi VisualNOS    ctermfg=255 guifg=#eeeeee ctermbg=60  guibg=#5f5f87
hi WildMenu     ctermfg=0   guifg=#000000 ctermbg=150 guibg=#afdf87 cterm=bold gui=bold

""" Syntax highlighting {{{2
hi Comment      ctermfg=244 guifg=#808080
hi Constant     ctermfg=229 guifg=#ffffaf
hi Identifier   ctermfg=182 guifg=#dfafdf                           cterm=none
hi Ignore       ctermfg=238 guifg=#444444
hi Number       ctermfg=180 guifg=#dfaf87
hi PreProc      ctermfg=150 guifg=#afdf87
hi Special      ctermfg=174 guifg=#df8787
hi Statement    ctermfg=110 guifg=#87afdf                           cterm=none gui=none
hi Type         ctermfg=146 guifg=#afafdf                           cterm=none gui=none

""" Special {{{2
""" .diff {{{3
hi diffAdded    ctermfg=150 guifg=#afdf87
hi diffRemoved  ctermfg=174 guifg=#df8787
""" vimdiff {{{3
hi diffAdd      ctermfg=bg  guifg=bg      ctermbg=151 guibg=#afdfaf
"hi diffDelete   ctermfg=bg  guifg=bg      ctermbg=186 guibg=#dfdf87 cterm=none gui=none
hi diffDelete   ctermfg=bg  guifg=bg      ctermbg=246 guibg=#949494 cterm=none gui=none
hi diffChange   ctermfg=bg  guifg=bg      ctermbg=181 guibg=#dfafaf
hi diffText     ctermfg=bg  guifg=bg      ctermbg=174 guibg=#df8787 cterm=none gui=none
""" HTML {{{3
" hi htmlTag      ctermfg=146  guifg=#afafdf
" hi htmlEndTag   ctermfg=146  guifg=#afafdf
hi htmlTag      ctermfg=244
hi htmlEndTag   ctermfg=244
hi htmlArg	ctermfg=182  guifg=#dfafdf
hi htmlValue	ctermfg=187  guifg=#dfdfaf
hi htmlTitle	ctermfg=254  ctermbg=95
" hi htmlArg	ctermfg=146
" hi htmlTagName	ctermfg=146
" hi htmlString	ctermfg=187
""" XML {{{3
hi link xmlTagName	Statement
" hi link xmlTag		Comment
" hi link xmlEndTag	Statement
hi link xmlTag		xmlTagName
hi link xmlEndTag	xmlTag
hi link xmlAttrib	Identifier
""" django {{{3
hi djangoVarBlock ctermfg=180  guifg=#dfaf87
hi djangoTagBlock ctermfg=150  guifg=#afdf87
hi djangoStatement ctermfg=146  guifg=#afafdf
hi djangoFilter ctermfg=174  guifg=#df8787
""" python {{{3
hi pythonExceptions ctermfg=174
""" NERDTree {{{3
hi Directory      ctermfg=110  guifg=#87afdf
hi treeCWD        ctermfg=180  guifg=#dfaf87
hi treeClosable   ctermfg=174  guifg=#df8787
hi treeOpenable   ctermfg=150  guifg=#afdf87
hi treePart       ctermfg=244  guifg=#808080
hi treeDirSlash   ctermfg=244  guifg=#808080
hi treeLink       ctermfg=182  guifg=#dfafdf
""" rst #{{{3
hi link rstEmphasis Number

""" VimDebug {{{3
" FIXME
" you may want to set SignColumn highlight in your .vimrc
" :help sign
" :help SignColumn

" hi currentLine term=reverse cterm=reverse gui=reverse
" hi breakPoint  term=NONE    cterm=NONE    gui=NONE
" hi empty       term=NONE    cterm=NONE    gui=NONE

" sign define currentLine linehl=currentLine
" sign define breakPoint  linehl=breakPoint  text=>>
" sign define both        linehl=currentLine text=>>
" sign define empty       linehl=empty
""" vimHelp {{{3
hi link helpExample Number
hi link helpNumber String
hi helpURL ctermfg=110 guifg=#87afdf                           cterm=underline gui=underline
hi link helpHyperTextEntry helpURL
