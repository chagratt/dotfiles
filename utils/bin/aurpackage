#!/usr/bin/env bash
set -euo pipefail

# simple script that helps me managing my packages from AUR
# dependancy : the jq utility to parse json fron AUR API
# (https://stedolan.github.io/jq/)

readonly TMPFILE=$(mktemp /tmp/aurupdate.XXXXXXXXX.txt)
readonly AUR_URL='https://aur.archlinux.org/rpc?v=5&'
readonly NEWER_FMT='\033[0;1m%s \033[0;31m%s\033[0m -> \033[32m%s\033[0m\n'
readonly OLDER_FMT='\033[0;1m%s \033[0m%s != %s\n'


function finish {
    rm -f "${TMPFILE}"
}
trap finish EXIT

check_updates() {
    # great snippet found at :
    # https://bbs.archlinux.org/viewtopic.php?pid=2071670#p2071670
    pacman -Qm | sort > "${TMPFILE}"
    curl -s "${AUR_URL}type=info$(printf '&arg[]=%s' $(cut -d ' ' -f 1 ${TMPFILE}))" \
        | jq -r '.results[]|.Name+" "+.Version' \
        | sort | join "${TMPFILE}" - \
        | while read pkg a b; do
            case "$(vercmp $a $b)" in
                -1) printf "${NEWER_FMT}" "$pkg" "$a" "$b" ;;
                1)  printf "${OLDER_FMT}" "$pkg" "$a" "$b" ;;
            esac
        done
}

# shellcheck disable=SC2221,SC2222
case ${1:-} in
    list)
        pacman -Qm
        ;;
    install)
        cat << EOTXT
Step 1 : Acquire build files
  $ git clone https://aur.archlinux.org/package_name.git
  From the package AUR page, labelled as the 'Git Clone URL' in the 'Package Details'

Step 1.5 : Check the files
  $ cd package_name
  $ less PKGBUILD
  Also check others files

Step 2 : Build and install the package
  $ makepkg -sirc

More information : https://wiki.archlinux.org/title/Arch_User_Repository#Installing_and_upgrading_packages
EOTXT
        ;;
    check)
        check_updates
        ;;
    *|help)
        cat << EOTXT
Usage : aurpackage <OPERATION>

OPERATION : install, list, check, help

install: prints install steps
list   : list packages installed from AUR
check  : check for AUR installed packages updates
help   : display this text
EOTXT
        ;;
esac

