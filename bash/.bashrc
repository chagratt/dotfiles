#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# ssh keys handling
eval $(keychain --eval --quiet --nogui --noask --inherit any-once)

# built-in bash 4+ shortener \w for $PS1 :)
export PROMPT_DIRTRIM=3

export HISTCONTROL=ignoreboth:erasedups
# tweak history format
export HISTTIMEFORMAT="%Y/%m/%d %H:%M:%S :: "
# Append commands to the bash command history file (~/.bash_history)
# instead of overwriting it.
shopt -s histappend
# share across all bash terms (see https://unix.stackexchange.com/a/18443 )
PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# auto prepend 'cd' when enteing path in the shell
shopt -s autocd

# colors for my prompts
# this is only a reminder beceause I don't use vars anymore
# Color_Off='\e[00m'
# Cyan='\e[0;36m'
# Blue='\e[0;34m'
# BBlue='\e[1;34m'
# BCyan='\e[1;36m'
# BRed is for root prompt :)
# BRed='\e[1;31m'
# ----

__cmd_err_code() {
    let ERRCODE=$?
    if [[ ${ERRCODE} -ne 0 ]]; then
        echo -e "\e[1;31m${ERRCODE}\e[00m"
    else
        echo ""
    fi
}

GIT_PS1_SHOWCOLORHINTS=
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_DESCRIBE_STYLE="branch"

[ -r /usr/share/git/git-prompt.sh  ] && . /usr/share/git/git-prompt.sh

PS1="\n\[\e[38;5;6m\] \u\[\e[00m\] \[\e[1;38;5;4m\]  \w\[\e[00m\]\$(__git_ps1) \$(__cmd_err_code)\n» "
#PS1="\n\[\e[38;5;0m\]\[\e[48;5;32m\]  \u \[\e[00m\]\[\e[38;5;0m\]\[\e[48;5;11m\]   \w \[\e[00m\]\$(__git_ps1) \$(__cmd_err_code)\n» "

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

# Add bash aliases.
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

if [ -f ~/MesDocs/Developpement/shell/goto/goto.sh ]; then
    source ~/MesDocs/Developpement/shell/goto/goto.sh
fi

