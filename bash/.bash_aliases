alias ls='ls -F --color=auto'
alias ll='ls -lF --color=auto'
alias lll='ls -AlrtFqsh --color=auto'
#alias tunnel='ssh -p 443 -D 2080 <user>@<IP>'
alias fwlog='journalctl -k | grep "IN=.*OUT=.*" | less'
alias orphelins='echo "pacman -Qdt" && pacman -Qdt'
alias notes="$EDITOR ~/MesDocs/Textes/_notes/index.md"
alias stripexif='exiftool -all= '
alias hs='hugo server -F -D --disableFastRender'

