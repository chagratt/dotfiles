#
# ~/.bash_profile
#

PATH=~/bin:$PATH

[[ -f ~/.bashrc ]] && . ~/.bashrc

export EDITOR="vim"
export TERM=xterm-256color
export MPC_FORMAT="%artist% : %title% - %album%"
export RIPGREP_CONFIG_PATH="$HOME/.ripgreprc"

# Wayland with Sway
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export MOZ_ENABLE_WAYLAND=1
    export XDG_CURRENT_DESKTOP=sway
    export XDG_SESSION_TYPE=wayland
#    export BEMENU_OPTS='--hb=#285577 --hf=#ffffff --tb=#285577 --tf=#ffffff --fn "JetBrains Mono 11"'
#    export BEMENU_BACKEND='wayland'
    exec sway
fi

